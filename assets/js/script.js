if (!( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
    $("#wrapper").toggleClass("toggled");
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

var waitForResizeToComplete = debounce(function() {
    // $("#wrapper").removeClass("toggled");
    // $("#wrapper").removeClass("right-toggled");
}, 250);

$(function() {

    var sidebarHovered = false;
    var risizeInProgress =false;

    $(window).on('resize', waitForResizeToComplete);

    $('.dropdown-menu.icon-action').on('click', function (e) {
        e.preventDefault();

        if ($(this).closest('li.dropdown').hasClass('show')) {
            $(this).closest('li.dropdown').toggleClass('show');
        }

    });

    $('body').on('click', function (e) {
        if (!$('li.dropdown').is(e.target) && $('.dropdown-menu').has(e.target).length === 0 && $('.show').has(e.target).length === 0) {
            $('li.dropdown').removeClass('show');
            $('.dropdown-menu').removeClass('show');
        }
    });

    $('.nav-link').on('click', function(e) {
        $('li.dropdown').removeClass('show');
        $('.dropdown-menu').removeClass('show');
    });

    $("#sidebar-toggle, #toggle-sidebar-toggler").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#wrapper").removeClass("right-toggled");
        $('.content-mask').hide();
    });

    $("#right-sidebar-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("right-toggled");
        $("#wrapper").removeClass("toggled");
        $('.content-mask').hide();
    });

    $('.sidebar-nav li a.nav-trigger').click(function(e) {
        e.preventDefault();

        $('.sidebar-nav li').removeClass('active');
        var parent = $(this).parent().parent();

        if (parent.hasClass('active')) {
            parent.find('.sidebar-menu-items').slideUp('fast', function() {
                parent.removeClass('active');
            });

        } else {
            parent.addClass('active');
            parent.find('.sidebar-menu-items').hide();
            parent.find('.sidebar-menu-items').slideDown('fast');
        }

    });

    $('.sidebar-nav').hover(function() {

        if (!$('#wrapper').hasClass('toggled')) {
            sidebarHovered = true;

            setTimeout(function() {
                $("#wrapper").toggleClass("toggled");
                $("#wrapper").toggleClass('overflow');
                $('body').toggleClass('body-mask');
                $('.content-mask').show();
            }, 200);
        }

    }, function() {

        if (sidebarHovered) {
            setTimeout(function() {
                $("#wrapper").toggleClass("toggled");
                $("#wrapper").toggleClass('overflow');
                $('body').toggleClass('body-mask');
                $('.content-mask').hide();
            }, 0);

            sidebarHovered = false;
        }

    });

});
